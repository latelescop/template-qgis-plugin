<!---
Before opening a new issue, make sure to verify the issue you're about to submit isn't a duplicate.
--->

## Environment

- Operating system:
- Cookiecutter version (`coookiecutter --version`):
- Cookiecutter installation mode (delete what does not apply or add what is missing): pipx / pip in system / pip in virtualv

## Summary

<!-- Summarize the bug encountered concisely. -->

### Steps to reproduce

<!-- Describe how one can reproduce the issue - this is very important.
Please consider using an ordered list.

IF RELEVANT, give the cookiecutter command you've been using and upload the resulted output by giving a name related to the issue. Example on Linux:

```sh
cd /tmp
cookiecutter --no-input -f https://gitlab.com/Oslandia/qgis/template-qgis-plugin plugin_name=issue_processing_option plugin_processing=y
zip -r templater_qgis_issue_processing_option.zip plugin_issue_processing_option/
```

Then upload the templater_qgis_issue_processing_option.zip file to the issue.

-->

Command to reproduce:

```sh
SEE TEMPLATE IN COMMENTS
```

### What is the current *bug* behavior?

<!-- Describe what actually happens. -->

### What is the expected *correct* behavior?

<!-- Describe what you should see instead. -->

### Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
 as it's tough to read otherwise. -->

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->

/label ~bug
