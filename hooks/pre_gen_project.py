#! python3  # noqa: E265

"""
    Executed before templating operations in cookiecutter.

    See: https://cookiecutter.readthedocs.io/en/stable/advanced/hooks.html
"""

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
import json
import logging
import sys
from pathlib import Path
from typing import Any, Dict

# ############################################################################
# ########## Globals ###############
# ##################################

# log customization
if "{{ cookiecutter.debug }}".startswith("y"):
    log_level = logging.DEBUG
else:
    log_level = logging.INFO

log_format = "[%(asctime)s] [%(levelname)s] - %(message)s"
logging.basicConfig(level=log_level, format=log_format, force=True)

if log_level == 10:
    logging.debug("Debug mode enabled")

# ############################################################################
# ########## Main ##################
# ##################################


def main():
    """Main script."""

    # Read the cookiecutter context
    cookiecutter_context: str = """{{cookiecutter | jsonify}}"""
    context: Dict[str, Any] = json.loads(cookiecutter_context)

    # check plugin name
    plugin_name_slug = context.get("plugin_name_slug", "")
    if (
        hasattr(plugin_name_slug, "isidentifier")
        and not plugin_name_slug.isidentifier()
    ):
        logging.critical(
            f"Project slug ('{plugin_name_slug}') is not a valid Python identifier."
        )
        sys.exit(
            "'{}' project slug is not a valid Python identifier.".format(
                plugin_name_slug
            )
        )

    if not plugin_name_slug == plugin_name_slug.lower():
        logging.critical(
            f"Project slug should be all lowercase. {plugin_name_slug} is not."
        )
        sys.exit(1)

    # check author name
    if "\\" in context.get("author_name"):
        logging.critical("Don't include backslashes in author name.")
        sys.exit(1)

    # check if icon exists or not
    plugin_icon = Path("{{ cookiecutter.plugin_icon }}")
    if not plugin_icon.is_file():
        logging.info("Icon doesn't exist. Fallback to default icon.")
        "{{ cookiecutter.update({ 'plugin_icon': 'default_icon.png' }) }}"

    # Define repository_url_issues from ci_ci_tool
    """
    {% if cookiecutter.ci_cd_tool|lower == "github" %}
    "{{ cookiecutter.update({ 'repository_url_issues': cookiecutter.repository_url_base+'/issues/'}) }}"
    {% elif cookiecutter.ci_cd_tool|lower == "gitlab" %}
    "{{ cookiecutter.update({ 'repository_url_issues': cookiecutter.repository_url_base+'/-/issues/'}) }}"
    {% else %}
    "{{ cookiecutter.update({ 'repository_url_issues': none }) }}"
    {% endif %}
    """

    # Define repository_url_pages to repository_url_base
    # To be defined from repository_url_base with regex if possible in jinja
    "{{ cookiecutter.update({ 'repository_url_pages': cookiecutter.repository_url_base}) }}"


# #############################################################################
# ##### Main #######################
# ##################################
if __name__ == "__main__":
    main()
