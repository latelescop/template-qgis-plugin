# Tests

## System requirements

```bash
python -m pip install -U -r requirements/testing.txt
```

## Run tests

Run all tests:

```bash
pytest
```

Run a specific test:

```bash
python -m unittest test.test_changelog
```
