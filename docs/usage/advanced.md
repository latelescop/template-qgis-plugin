# Advanced usage

## Use without any prompt

It's possible to pass values directly through the CLI, avoiding the prompts:

```bash
cookiecutter --no-input -f https://gitlab.com/Oslandia/qgis/template-qgis-plugin plugin_name=my_latest_plugin
```

## Use a configuration file

Cookiecutter allow using a [configuration file](https://cookiecutter.readthedocs.io/en/stable/advanced/user_config.html), using the [YAML](https://wikipedia.org/wiki/YAML/) syntax and allowing to set the default context and global options. By default, it's a `.cookiecutterrc` file in the home directory but it can be a custom file.

For example, create a file `ck-qgis-plugins.yaml`:

```yaml
default_context:
    author_email: julien.moura@oslandia.com
    author_name: Julien Moura
    author_org: Oslandia
    open_source_license: GPLv2+
    plugin_category: Vector
    qgis_version_min: "3.22"
```

Then use it:

```bash
cookiecutter --config-file ck-qgis-plugins.yaml -f https://gitlab.com/Oslandia/qgis/template-qgis-plugin
```

See the [related official documentation to get more details](https://cookiecutter.readthedocs.io/en/stable/advanced/user_config.html).
